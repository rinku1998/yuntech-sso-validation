import cv2
import numpy as np
import os


def dHash(img, width=9, height=8):
    img=cv2.resize(img,(width, height),interpolation=cv2.INTER_CUBIC)
    # gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    dhash_string=''
    for i in range(height):               
        for j in range(height):
            if img[i, j]>img[i, j+1]:
                dhash_string += '1'
            else:
                dhash_string += '0'
    return dhash_string

def cmpHash(hash1, hash2):              
    n = 0
    if len(hash1)!=len(hash2):         
        return -1
    for i in range(len(hash1)):        
        if hash1[i]!=hash2[i]:
            n += 1
    return n


def compare(character):
    images = os.listdir('characters')
    result = []
    hash_source = dHash(character)
    for img in images:
        number = cv2.imread(os.path.join('characters', img), 0)
        hash_number = dHash(number)
        result.append(cmpHash(hash_source, hash_number))
    index = np.argmin(np.array(result))
    # print(np.argmax(np.array(result)))
    # print(result)
    return (images[index].split('.')[0])
